# N00B Networking

---

## What is it?
-- N00B Networking is a small project that focuses on the networking side of computers.

## How can I use it?
-- I really don't know. Use it how you want to as it uses the GNU license.

## What it includes:
-- PYTHON
chattycl.py
chattysv.py
ghbn.py
gnbh.py

-- SHELL SCRIPT
Client.sh
Server.sh